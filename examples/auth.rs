use myanimelist_rs::auth::{Auth, open_in_browser, redirect_server};
use serde_json;

/// Test the auth flow, and print the auth object as JSON
/// This auth object can be provided as the environment variable "MAL_TEST_AUTH" for running other unit tests
fn main() {
    let user_agent =  std::env::var("MAL_TEST_USER_AGENT").unwrap();
    let client_id =  std::env::var("MAL_TEST_CLIENT_ID").unwrap();
    let redirect_url = std::env::var("MAL_TEST_REDIRECT_URL").unwrap();
    let server_address = std::env::var("MAL_TEST_SERVER_ADDRESS").unwrap();

    // construct authentication
    let auth = Auth::new(user_agent.clone(), client_id, None, redirect_url);

    // get the url to redirect the user to for authentication
    let url = auth.get_auth_url();

    // the open in browser helper method will work on native windows,linux,macos apps, and will open the user's default browser
    // apps in different environments will need a different strategy
    open_in_browser(url.as_str()).unwrap();

    // We need a way to receive the redirect from the web browser.
    // This server is a very minimal, non-robust and non-secure HTTP server.
    // It binds to localhost:someport and waits for exactly one request (the redirect)
    //   then and parses out the authentication information.
    // This is not recommended for apps for a few reason
    //  - It is insecure
    //    - Does not implement HTTPS
    //    - Subject to man-in-the-middle attacks (eg. malware/DNS could rebind localhost)
    //  - Not robust
    //    - There are a limited number of redirect addresses MAL and you can agree upon when generating your client id
    //    - You must choose a listening port that is not already in use.
    //    - May not be able to choose a port that is both free and registered with MAL
    //    - If a request other than the intended redirect request is made, it will fail, and will not detect it.
    //
    // Instead, look into other methods of receiving the redirect.
    // https://tools.ietf.org/html/rfc6749
    let mut auth = redirect_server::Server::new(user_agent, server_address, auth).go().unwrap();
    auth.get_access_token().unwrap();

    // the token can be refreshed (a waste if it was generated, such as in this base)
    auth.refresh().unwrap();

    println!("{}", serde_json::to_string(&auth).unwrap());
}