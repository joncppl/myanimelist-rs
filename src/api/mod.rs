/// Anime API endpoints
pub mod anime;

/// User AnimeList API endpoints
pub mod user_animelist;

/// Manga API endpoints
pub mod manga;

/// User MangaList API endpoints
pub mod user_mangalist;

/// User API endpoints
pub mod user;

use serde::{Serialize, Deserialize};
use crate::auth::Auth;

pub const BASE_URL: &str = "https://api.myanimelist.net/v2";

#[derive(Debug)]
pub enum Error {
    NoAuth,
    TimedOut,
    Unknown,
    NoBody,
    ParseError(serde_json::Error),
    QuerySerializeError(serde_urlencoded::ser::Error),
    HttpError(reqwest::StatusCode),
}

impl From<reqwest::Error> for Error {
    fn from(e: reqwest::Error) -> Self {
        if e.is_timeout() {
            Error::TimedOut
        } else {
            Error::Unknown
        }
    }
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Error::ParseError(e)
    }
}

impl From<serde_urlencoded::ser::Error> for Error {
    fn from(e: serde_urlencoded::ser::Error) -> Self {
        Error::QuerySerializeError(e)
    }
}

#[derive(Debug)]
pub(crate) struct PerformResponse {
    status: reqwest::StatusCode,
    body: Option<String>,
}

pub(crate) fn apply_general_headers(req: reqwest::blocking::RequestBuilder) -> reqwest::blocking::RequestBuilder {
    req.header(reqwest::header::ACCEPT, "application/json")
        .header(reqwest::header::CONTENT_TYPE, "application/x-www-form-urlencoded")
}

pub(crate) fn apply_auth_headers(req: reqwest::blocking::RequestBuilder, auth: &Auth) -> Result<reqwest::blocking::RequestBuilder, Error> {
    if let Some(token) = auth.token() {
        Ok(req.header(reqwest::header::AUTHORIZATION, format!("Bearer {}", token.token.access_token)))
    } else {
        Err(Error::NoAuth)
    }
}

pub(crate) fn perform_get<U: reqwest::IntoUrl>(
    url: U,
    auth: &Auth,
) -> Result<PerformResponse, Error> {
    let request = reqwest::blocking::ClientBuilder::new()
        .user_agent(auth.user_agent())
        .build()?
        .get(url);
    let request = apply_auth_headers(apply_general_headers(request), auth)?;
    let response = request.send()?;
    let status = response.status();
    Ok(PerformResponse { status, body: if let Ok(body) = response.text() { Some(body) } else { None } })
}

pub(crate) fn perform_patch<U: reqwest::IntoUrl, B: Serialize>(
    url: U,
    auth: &Auth,
    body: &B,
) -> Result<PerformResponse, Error> {
    let request = reqwest::blocking::ClientBuilder::new()
        .user_agent(auth.user_agent())
        .build()?
        .patch(url)
        .body(serde_urlencoded::to_string(body)?);
    let request = apply_auth_headers(apply_general_headers(request), auth)?;
    let response = request.send()?;
    let status = response.status();
    Ok(PerformResponse { status, body: if let Ok(body) = response.text() { Some(body) } else { None } })
}

pub(crate) fn perform_delete<U: reqwest::IntoUrl>(
    url: U,
    auth: &Auth,
) -> Result<PerformResponse, Error> {
    let request = reqwest::blocking::ClientBuilder::new()
        .user_agent(auth.user_agent())
        .build()?
        .delete(url);
    let request = apply_auth_headers(apply_general_headers(request), auth)?;
    let response = request.send()?;
    let status = response.status();
    Ok(PerformResponse { status, body: if let Ok(body) = response.text() { Some(body) } else { None } })
}

pub(crate) fn handle_response<'de, D: Deserialize<'de>>(res: &'de PerformResponse) -> Result<D, Error> {
    if !res.status.is_success() {
        return Err(Error::HttpError(res.status));
    }
    if let Some(body) = &res.body {
        Ok(serde_json::from_str::<D>(&body)?)
    } else {
        Err(Error::NoBody)
    }
}