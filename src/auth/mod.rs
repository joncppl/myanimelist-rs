/// Structures/Methods for performing Authentication flow
use serde::{Serialize, Deserialize};
use serde_urlencoded;
use serde_json;
use std::str::FromStr;

/// Old and dangerous way of doing authentication.
/// Generally, don't use this for a deployed application.
/// It should be fine for testing.
#[deprecated(note = "The authorization flow used by these methods in insecure for general applications. It is no longer documented by MAL, and therefore assumed to be no longer supported and may stop working at any time. In addition, repeated authorization failure will block your API client_id for a while.")]
pub mod old_style;

const AUTH_URL: &str = "https://myanimelist.net/v1/oauth2/authorize";
const TOKEN_URL: &str = "https://myanimelist.net/v1/oauth2/token";

/// Error type for Authorization methods
#[derive(Clone, Debug)]
pub enum AuthError {
    UnknownError,
    NetworkTimeout,
    InvalidResponse(String),
    AuthNotPresent,
    TokenNotPresent,
}

impl From<reqwest::Error> for AuthError {
    fn from(e: reqwest::Error) -> Self {
        if e.is_builder() || e.is_redirect() || e.is_status() {
            AuthError::UnknownError
        } else if e.is_timeout() {
            AuthError::NetworkTimeout
        } else {
            AuthError::UnknownError
        }
    }
}

/// An Authorization Token
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Token {
    /// Token Type
    pub token_type: String,
    /// When (in seconds) that the token will expire (relative to when it was created)
    pub expires_in: u64,
    /// Access token (used for authorizing future requests)
    pub access_token: String,
    /// Refresh token (used for generating a new `Token`, eg. after expiry)
    pub refresh_token: String,
}

/// Wraps a token, including the time it was generated
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TokenWrapper {
    /// The token
    pub token: Token,
    /// The time that the token was generated (unix time)
    pub generate_time: u64,
}

impl TokenWrapper {
    fn sec_since_epoch() -> u64 {
        std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap().as_secs()
    }

    /// Construct a new token wrapper, setting generated time to now.
    pub fn new(token: Token) -> Self {
        Self::new_with_timestamp(token, Self::sec_since_epoch())
    }

    /// Construct a new token wrapper, setting timestamp to the one provided
    pub fn new_with_timestamp(token: Token, unix_timestamp: u64) -> Self {
        Self {
            token,
            generate_time: unix_timestamp,
        }
    }

    /// Check if the token has expired
    pub fn expired(&self) -> bool {
        let now = Self::sec_since_epoch();
        now >= self.generate_time + self.token.expires_in
    }

    /// Get seconds until expiry (None if already expired)
    pub fn expires_in_secs(&self) -> Option<u64> {
        let now = Self::sec_since_epoch();
        let sec_since_generated = now - self.generate_time;
        if now >= self.generate_time + self.token.expires_in {
            None
        } else {
            Some(self.token.expires_in - sec_since_generated)
        }
    }

    /// Get time that the token will expire (None if already expired)
    pub fn expire_time(&self) -> Option<std::time::SystemTime> {
        if let Some(secs) = self.expires_in_secs() {
            Some(std::time::SystemTime::now() + std::time::Duration::from_secs(secs))
        } else {
            None
        }
    }
}

const CODE_CHALLENGE_LENGTH: usize = 100;

/// Main Structure for performing OAuth2.
/// Authorization redirection is required.
/// It is left to the application to implement that flow.
#[derive(Clone, Serialize, Deserialize)]
pub struct Auth {
    pub client_id: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub client_secret: Option<String>,
    pub redirect_url: String,
    pub user_agent: String,
    pub challenge: String,
    pub state: String,
    pub auth_code: Option<String>,
    pub token: Option<TokenWrapper>,
}

impl Auth {
    /// Start a new authorization flow
    /// # Parameters
    ///  * `user_agent` - The user agent for HTTP requests
    ///  * `client_id` - Your MAL Client ID
    ///  * `client_secret` - Your MAL Client Secret
    ///  * `redirect_url` - The URL to redirect the user to with the authorization result
    pub fn new<A: ToString, B: ToString, C: ToString>(
        user_agent: A,
        client_id: B,
        client_secret: Option<String>,
        redirect_url: C,
    ) -> Self {
        Auth {
            client_id: client_id.to_string(),
            client_secret: if let Some(cs) = client_secret { Some(cs) } else { None },
            redirect_url: redirect_url.to_string(),
            user_agent: user_agent.to_string(),
            challenge: unsafe { std::str::from_utf8_unchecked(&Self::gen_challenge()) }.to_string(),
            state: "AUTHSTART".to_string(),
            auth_code: None,
            token: None,
        }
    }

    pub fn user_agent(&self) -> &String {
        return &self.user_agent;
    }

    fn gen_challenge() -> [u8; CODE_CHALLENGE_LENGTH] {
        use rand::prelude::*;
        let mut rng = rand::rngs::StdRng::from_entropy();
        let mut bytes: [u8; 256] = [0; 256];
        let mut ret: [u8; CODE_CHALLENGE_LENGTH] = [0; CODE_CHALLENGE_LENGTH];
        let mut ret_i: usize = 0;
        // since we are only grabbing url safe characters,
        // it is likely we will need more randomness
        // hence we repeat until we fill the buffer.
        while ret_i < CODE_CHALLENGE_LENGTH {
            rng.fill_bytes(bytes.as_mut());
            let mut bytes_i: usize = 0;
            while bytes_i < 256 && ret_i < CODE_CHALLENGE_LENGTH {
                let c = bytes[bytes_i];
                match c as char { // only grab url safe characters
                    'a'..='z' | 'A'..='Z' | '0'..='9' => {
                        ret[ret_i] = c;
                        ret_i += 1;
                    }
                    _ => ()
                }
                bytes_i += 1;
            }
        }
        ret
    }

    /// Get an authorization url
    /// It is the application's responsibility to redirect the user to this address in a web browser.
    pub fn get_auth_url(&self) -> url::Url {
        #[derive(Serialize, Debug)]
        struct AuthQuery {
            response_type: String,
            client_id: String,
            code_challenge: String,
            state: String,
            redirect_url: String,
            code_challenge_method: String,
        }

        let auth_query = AuthQuery {
            response_type: "code".to_string(),
            client_id: self.client_id.clone(),
            code_challenge: self.challenge.clone(),
            state: self.state.to_string(),
            redirect_url: self.redirect_url.clone(),
            code_challenge_method: "plain".to_string(),
        };

        url::Url::from_str(&format!("{}?{}", AUTH_URL, serde_urlencoded::to_string(auth_query).unwrap())).unwrap()
    }

    /// Provide the query string (part of URL after '?') of the authorization's redirection url here.
    pub fn parse_redirect_query_string(&mut self, query_string: &str) -> Result<(), AuthError> {
        #[derive(Deserialize, Debug)]
        struct AuthResponse {
            code: String,
            state: String,
        }

        // TODO: errors
        let auth_response = match serde_urlencoded::from_str::<AuthResponse>(query_string) {
            Ok(r) => r,
            Err(e) => {
                return Err(AuthError::InvalidResponse(e.to_string()));
            }
        };
        if auth_response.state != self.state {
            return Err(AuthError::InvalidResponse("State Mismatch".to_string()));
        }
        self.auth_code = Some(auth_response.code);
        Ok(())
    }

    /// Get the query string to be provided to token request endpoint.
    pub fn get_token_query_string(&self) -> Result<String, AuthError> {
        #[derive(Serialize, Debug)]
        struct TokenRequest {
            client_id: String,
            #[serde(skip_serializing_if = "Option::is_none")]
            client_secret: Option<String>,
            code: String,
            code_verifier: String,
            grant_type: String,
        }

        if self.auth_code.is_none() {
            return Err(AuthError::AuthNotPresent);
        }

        let query = TokenRequest {
            client_id: self.client_id.clone(),
            client_secret: None,
            code: self.auth_code.as_ref().unwrap().clone(),
            code_verifier: self.challenge.clone(),
            grant_type: "authorization_code".to_string(),
        };

        Ok(serde_urlencoded::to_string(query).unwrap())
    }

    /// Get access token
    pub fn get_access_token(&mut self) -> Result<(), AuthError> {
        let request = reqwest::blocking::ClientBuilder::new()
            .user_agent(&self.user_agent)
            .build()?
            .post(TOKEN_URL)
            .header(reqwest::header::ACCEPT, "application/json")
            .header(reqwest::header::CONTENT_TYPE, "application/x-www-form-urlencoded")
            .body(self.get_token_query_string()?);

        let response = request.send()?;
        let success = response.status().is_success();
        let body = response.text()?;
        self.handle_response(success, &body)
    }

    /// Get access token (async)
    pub async fn get_access_token_async(&mut self) -> Result<(), AuthError> {
        let request = reqwest::ClientBuilder::new()
            .user_agent(&self.user_agent)
            .build()?
            .post(TOKEN_URL)
            .header(reqwest::header::ACCEPT, "application/json")
            .header(reqwest::header::CONTENT_TYPE, "application/x-www-form-urlencoded")
            .body(self.get_token_query_string()?);

        let response = request.send().await?;
        let success = response.status().is_success();
        let body = response.text().await?;
        self.handle_response(success, &body)
    }

    /// Handle a response for get access token
    pub fn handle_response(&mut self, success: bool, body: &str) -> Result<(), AuthError> {
        if success {
            match serde_json::from_str::<Token>(body) {
                Ok(result) => {
                    self.token = Some(TokenWrapper::new(result));
                    Ok(())
                }
                Err(e) => Err(AuthError::InvalidResponse(e.to_string()))
            }
        } else {
            Err(AuthError::UnknownError)
        }
    }

    /// Get a token reference
    pub fn token(&self) -> Option<&TokenWrapper> {
        self.token.as_ref()
    }

    pub fn get_token_refresh_query_string(&self) -> Result<String, AuthError> {
        #[derive(Serialize, Debug)]
        struct TokenRequest {
            client_id: String,
            #[serde(skip_serializing_if = "Option::is_none")]
            client_secret: Option<String>,
            code: String,
            code_verifier: String,
            grant_type: String,
            refresh_token: String,
        }

        if self.auth_code.is_none() {
            return Err(AuthError::AuthNotPresent);
        }

        if self.token.is_none() {
            return Err(AuthError::TokenNotPresent);
        }

        let query = TokenRequest {
            client_id: self.client_id.clone(),
            client_secret: None,
            code: self.auth_code.as_ref().unwrap().clone(),
            code_verifier: self.challenge.clone(),
            grant_type: "refresh_token".to_string(),
            refresh_token: self.token.as_ref().unwrap().token.refresh_token.clone(),
        };

        Ok(serde_urlencoded::to_string(query).unwrap())
    }

    /// Refresh the token
    pub fn refresh(&mut self) -> Result<(), AuthError> {
        let request = reqwest::blocking::ClientBuilder::new()
            .user_agent(&self.user_agent)
            .build()?
            .post(TOKEN_URL)
            .header(reqwest::header::ACCEPT, "application/json")
            .header(reqwest::header::CONTENT_TYPE, "application/x-www-form-urlencoded")
            .body(self.get_token_refresh_query_string()?);

        let response = request.send()?;
        let success = response.status().is_success();
        let body = response.text()?;
        self.handle_response(success, &body)
    }

    /// Refresh the token (async)
    pub async fn refresh_async(&mut self) -> Result<(), AuthError> {
        let request = reqwest::ClientBuilder::new()
            .user_agent(&self.user_agent)
            .build()?
            .post(TOKEN_URL)
            .header(reqwest::header::ACCEPT, "application/json")
            .header(reqwest::header::CONTENT_TYPE, "application/x-www-form-urlencoded")
            .body(self.get_token_refresh_query_string()?);

        let response = request.send().await?;
        let success = response.status().is_success();
        let body = response.text().await?;
        self.handle_response(success, &body)
    }
}

/// Utility to open url in a browser
#[cfg(target_os = "windows")]
#[allow(dead_code)]
pub fn open_in_browser<T: ToString>(url: T) -> Result<(), ()> {
    let url_cstr = std::ffi::CString::new(url.to_string().as_str()).unwrap();
    unsafe {
        // providing an http url to ShellExecute will open it in Window's default browser
        if winapi::um::shellapi::ShellExecuteA(
            std::ptr::null_mut() as *mut _,
            std::ptr::null() as *const _,
            url_cstr.as_ptr(),
            std::ptr::null() as *const _,
            std::ptr::null() as *const _,
            winapi::um::winuser::SW_SHOW,
        ) as i32 > 32 {
            Ok(())
        } else {
            Err(())
        }
    }
}

/// Utility to open url in a browser
#[cfg(target_os = "linux")]
#[allow(dead_code)]
pub fn open_in_browser<T: ToString>(url: T) -> Result<(), ()> {
    use std::process::Command;

    match Command::new("gio").args(&["open", &url.to_string()]).output() {
        Ok(_) => return Ok(()),
        Err(_) => ()
    };
    match Command::new("xdg-open").args(&[&url.to_string()]).output() {
        Ok(_) => Ok(()),
        Err(_) => Err(())
    }
}

/// Utility to open url in a browser
#[cfg(target_os = "macos")]
#[allow(dead_code)]
pub fn open_in_browser<T: ToString>(url: T) -> Result<(), ()> {
    use std::process::Command;
    match Command::new("open").args(&[&url.to_string()]).output() {
        Ok(_) => return Ok(()),
        Err(_) => Err(())
    }
}

/// A minimum-viable HTTP server on host.
/// example host: 127.0.0.1:3000
/// blocks until exactly one request is received, assumes it is the auth redirect, parses it,
/// and returns the authorization
#[allow(dead_code)]
pub mod redirect_server {
    /// A minimum-viable HTTP server on host.
    /// example host: 127.0.0.1:3000
    /// blocks until exactly one request is received, assumes it is the auth redirect, parses it,
    /// and returns the authorization
    pub struct Server {
        host: String,
        auth: super::Auth,
        app_name: String,
    }

    /// Error type for server methods
    #[derive(Debug)]
    pub enum ServerError {
        IOError(std::io::Error),
        HTTParseError(httparse::Error),
        InvalidRequestURL(String),
        AuthError(super::AuthError),
    }

    impl From<std::io::Error> for ServerError {
        fn from(e: std::io::Error) -> Self {
            ServerError::IOError(e)
        }
    }

    impl From<httparse::Error> for ServerError {
        fn from(e: httparse::Error) -> Self {
            ServerError::HTTParseError(e)
        }
    }

    impl From<super::AuthError> for ServerError {
        fn from(e: super::AuthError) -> Self {
            ServerError::AuthError(e)
        }
    }

    impl Server {
        /// Create the server
        pub fn new<A: ToString, B: ToString>(app_name: A, host: B, auth: super::Auth) -> Self {
            Server { host: host.to_string(), auth, app_name: app_name.to_string() }
        }

        /// Run the server.
        /// Blocks until it receives exactly one request,
        /// attempts to parse authorization out of it.
        pub fn go(self) -> Result<super::Auth, ServerError> {
            use std::io::prelude::*;
            use std::net::TcpListener;

            let listener = TcpListener::bind(&self.host)?;
            let mut socket_stream = listener.incoming().next().unwrap()?;

            // read all bytes of the request
            let mut request_bytes = Vec::new();
            loop {
                const BUF_SIZE: usize = 4096;
                let mut buf: [u8; BUF_SIZE] = [0; BUF_SIZE];
                match socket_stream.read(&mut buf) {
                    Ok(val) => if val > 0 {
                        request_bytes.append(&mut Vec::from(&buf[0..val]));
                        if val < BUF_SIZE {
                            break;
                        }
                    } else {
                        break;
                    },
                    Err(e) => panic!("{}", e)
                };
            }

            let mut headers = [httparse::EMPTY_HEADER; 16];
            let mut parsed_request = httparse::Request::new(&mut headers);

            parsed_request.parse(&request_bytes)?;

            let raw_url = if let Some(path) = parsed_request.path {
                format!("http://{}{}", self.host, path)
            } else {
                return Err(ServerError::InvalidRequestURL("".to_string()));
            };


            let parsed_url = match url::Url::parse(&raw_url) {
                Ok(url) => url,
                Err(_) => return Err(ServerError::InvalidRequestURL(raw_url))
            };

            let query = if let Some(query) = parsed_url.query() {
                query
            } else {
                return Err(ServerError::InvalidRequestURL("No query string".to_string()));
            };

            let mut ret_auth = self.auth;

            ret_auth.parse_redirect_query_string(query)?;

            // return a minimal http response to the browser
            let r = format!("HTTP/1.1 200 OK\r\n\r\n<html><head><title>{} Authorized</title></head><body>{} Authorized</body></html>", self.app_name, self.app_name);
            socket_stream.write(r.as_bytes())?;
            socket_stream.flush()?;

            Ok(ret_auth)
        }
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn test_get_auth_url() {
        let client_id = std::env::var("MAL_TEST_CLIENT_ID").unwrap();
        let redirect_url = "this://is/a/url/maybe";
        let url = Auth::new("ACC", &client_id, None, redirect_url).get_auth_url();

        assert_eq!(url.scheme(), "https");
        assert_eq!(url.domain().unwrap(), "myanimelist.net");
        assert_eq!(url.path(), "/v1/oauth2/authorize");
        let mut qs = url.query_pairs();

        let mut got_response_type = false;
        let mut got_client_id = false;
        let mut got_code_challenge = false;
        let mut got_state = false;
        let mut got_redirect_url = false;
        let mut got_code_challenge_method = false;
        while let Some((key, val)) = qs.next() {
            match key.as_ref() {
                "response_type" => {
                    assert_eq!(val, "code");
                    got_response_type = true;
                }
                "client_id" => {
                    assert_eq!(val, client_id);
                    got_client_id = true;
                }
                "code_challenge" => {
                    assert_eq!(val.len(), CODE_CHALLENGE_LENGTH);
                    got_code_challenge = true;
                }
                "state" => {
                    assert_eq!(val, "AUTHSTART");
                    got_state = true;
                }
                "redirect_url" => {
                    assert_eq!(val, redirect_url);
                    got_redirect_url = true;
                }
                "code_challenge_method" => {
                    assert_eq!(val, "plain");
                    got_code_challenge_method = true;
                }
                _ => panic!("Unknown query string key {}", key)
            }
        }
        assert!(got_response_type);
        assert!(got_client_id);
        assert!(got_code_challenge);
        assert!(got_state);
        assert!(got_redirect_url);
        assert!(got_code_challenge_method);
    }

    // actually tested in bin/auth.rs
    // since it is more suitable to being executed manually.

    /// Get the auth object from the environment variable "MAL_TEST_AUTH"
    pub fn get_auth_from_env() -> Auth {
        let raw = std::env::var("MAL_TEST_AUTH").unwrap();
        let mut auth = serde_json::from_str::<Auth>(&raw).unwrap();
        let needs_refresh = if let Some(token) = &auth.token {
            token.expired()
        } else {
            panic!("\"MAL_TEST_AUTH\" has no token");
        };
        if needs_refresh {
            auth.refresh().unwrap();
        }
        auth
    }
}

