use crate::auth::Auth;
use super::Error;
use crate::api_objects::*;
use super::{perform_get, handle_response, BASE_URL};
use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
pub struct GetAnimeListQuery {
    pub q: String,
    pub limit: u64,
    pub offset: u64,
    pub nsfw: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<String>,
}

pub fn get_anime_list(query: &GetAnimeListQuery, auth: &Auth) -> Result<PageableData<Vec<Node<Anime>>>, Error> {
    let response = perform_get(
        &format!("{}/anime?{}", BASE_URL, serde_urlencoded::to_string(query)?),
        auth,
    )?;
    handle_response(&response)
}

#[derive(Clone, Debug, Serialize)]
pub struct GetAnimeDetailQuery {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<String>,
    pub nsfw: bool,
}

pub fn get_anime_details(anime_id: u64, query: &GetAnimeDetailQuery, auth: &Auth) -> Result<Anime, Error> {
    let response = perform_get(
        &format!("{}/anime/{}?{}", BASE_URL, anime_id, serde_urlencoded::to_string(query)?),
        auth,
    )?;
    handle_response(&response)
}

#[derive(Clone, Debug, Serialize)]
pub struct GetAnimeRankingQuery {
    pub ranking_type: AnimeRankingType,
    pub limit: u64,
    pub offset: u64,
    pub nsfw: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<String>,
}

pub fn get_anime_ranking(query: &GetAnimeRankingQuery, auth: &Auth) -> Result<PageableData<Vec<RankingAnimePair>>, Error> {
    let response = perform_get(
        &format!("{}/anime/ranking?{}", BASE_URL, serde_urlencoded::to_string(query)?),
        auth,
    )?;
    handle_response(&response)
}

#[derive(Clone, Debug, Serialize)]
pub struct GetSeasonalAnimeQuery {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sort: Option<SortStyle>,
    pub limit: u64,
    pub offset: u64,
    pub nsfw: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<String>,
}

pub fn get_seasonal_anime(season: &Season, query: &GetSeasonalAnimeQuery, auth: &Auth) -> Result<PageableData<Vec<Node<Anime>>>, Error> {
    let season_ident = season.season.clone();
    let season_str: &'static str = season_ident.into();
    let response = perform_get(
        &format!("{}/anime/season/{}/{}?{}", BASE_URL, season.year, season_str, serde_urlencoded::to_string(query)?),
        auth,
    )?;
    handle_response(&response)
}

#[derive(Clone, Debug, Serialize)]
pub struct GetSuggestedAnime {
    pub limit: u64,
    pub offset: u64,
    pub nsfw: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<String>,
}

pub fn get_suggested_anime(query: &GetSuggestedAnime, auth: &Auth) -> Result<PageableData<Vec<Node<Anime>>>, Error> {
    let response = perform_get(
        &format!("{}/anime/suggestions?{}", BASE_URL, serde_urlencoded::to_string(query)?),
        auth,
    )?;
    handle_response(&response)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_anime_list() {
        let auth = crate::auth::tests::get_auth_from_env();
        let query = GetAnimeListQuery {
            q: "Code Geass".to_string(),
            limit: 2,
            offset: 0,
            nsfw: false,
            fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
        };
        let result = get_anime_list(&query, &auth).unwrap();
        println!("{:#?}", result);
        assert!(result.data.len() > 0);
    }

    #[test]
    fn test_get_anime_details() {
        let auth = crate::auth::tests::get_auth_from_env();
        let query = GetAnimeDetailQuery {
            fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
            nsfw: false,
        };
        let result = get_anime_details(1, &query, &auth).unwrap();
        println!("{:#?}", result);
        assert_eq!(result.title, "Cowboy Bebop");
    }

    #[test]
    fn test_get_anime_ranking() {
        let auth = crate::auth::tests::get_auth_from_env();
        let query = GetAnimeRankingQuery {
            ranking_type: AnimeRankingType::All,
            limit: 100,
            offset: 0,
            nsfw: false,
            fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
        };
        let result = get_anime_ranking(&query, &auth).unwrap();
        println!("{:#?}", result);
        assert!(result.data.len() > 0);
    }

    #[test]
    fn test_get_seasonal_anime() {
        let auth = crate::auth::tests::get_auth_from_env();
        let query = GetSeasonalAnimeQuery {
            sort: None,
            limit: 100,
            offset: 0,
            nsfw: false,
            fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string())
        };
        let season = Season {
            year: 2020,
            season: SeasonIdent::Winter
        };
        let result = get_seasonal_anime(&season, &query, &auth).unwrap();
        println!("{:#?}", result);
        assert!(result.data.len() > 0);
    }

    #[test]
    fn test_get_suggested_anime() {
        let auth = crate::auth::tests::get_auth_from_env();
        let query = GetSuggestedAnime {
            limit: 100,
            offset: 0,
            nsfw: false,
            fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string())
        };
        let result = get_suggested_anime(&query, &auth).unwrap();
        println!("{:#?}", result);
        assert!(result.data.len() > 0);
    }
}