use crate::auth::Auth;
use super::Error;
use crate::api_objects::*;
use super::{perform_get, perform_patch, perform_delete, handle_response, BASE_URL};
use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
pub struct UpdateUserMangaStatus {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<UserReadStatus>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_rereading: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub score: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub num_volumes_read: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub num_chapters_read: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub priority: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub num_times_reread: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reread_value: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tags: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comments: Option<String>,
}

pub fn update_manga_list_status(manga_id: u64, update: &UpdateUserMangaStatus, auth: &Auth) -> Result<UserMangaListStatus, Error> {
    let response = perform_patch(
        &format!("{}/manga/{}/my_list_status", BASE_URL, manga_id),
        auth,
        update,
    )?;
    handle_response(&response)
}

pub fn delete_manga_from_list(manga_id: u64, auth: &Auth) -> Result<(), Error> {
    let response = perform_delete(
        &format!("{}/manga/{}/my_list_status", BASE_URL, manga_id),
        auth,
    )?;
    if response.status.is_success() {
        Ok(())
    } else {
        Err(Error::HttpError(response.status))
    }
}

#[derive(Clone, Debug, Serialize)]
pub struct GetUserMangaListQuery {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<UserStatus>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sort: Option<SortStyle>,
    pub limit: u64,
    pub offset: u64,
    pub nsfw: bool,
}

pub fn get_user_manga_list<U: ToString>(user: U, query: &GetUserMangaListQuery, auth: &Auth) -> Result<PageableData<Vec<Node<Manga>>>, Error> {
    let response = perform_get(
        &format!("{}/users/{}/mangalist?{}",
                 BASE_URL,
                 user.to_string(),
                 serde_urlencoded::to_string(query)?
        ),
        auth)?;
    handle_response(&response)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_update_manga_list() {
        let auth = crate::auth::tests::get_auth_from_env();
        let query = UpdateUserMangaStatus {
            status: Some(UserReadStatus::Reading),
            is_rereading: None,
            score: None,
            num_volumes_read: Some(1),
            num_chapters_read: Some(2),
            priority: None,
            num_times_reread: None,
            reread_value: None,
            tags: None,
            comments: None,
        };
        let manga_id = 1; // https://myanimelist.net/manga/1/Monster
        let result = update_manga_list_status(manga_id, &query, &auth).unwrap();
        println!("{:#?}", result);
        assert_eq!(result.num_volumes_read, 1);
        assert_eq!(result.num_chapters_read, 2);
    }

    #[test]
    fn test_delete_manga_from_list() {
        let auth = crate::auth::tests::get_auth_from_env();
        let manga_id = 1; // https://myanimelist.net/manga/1/Monster
        delete_manga_from_list(manga_id, &auth).unwrap();
    }

    #[test]
    fn test_get_usermanga_list() {
        let auth = crate::auth::tests::get_auth_from_env();
        let query = GetUserMangaListQuery {
            fields: Some(crate::api_objects::ALL_ANIME_AND_MANGA_FIELDS.to_string()),
            status: None,
            sort: None,
            limit: 100,
            offset: 0,
            nsfw: true,
        };
        let result = get_user_manga_list("@me", &query, &auth).unwrap();
        println!("{:#?}", result);
        assert!(result.data.len() > 0);
    }
}