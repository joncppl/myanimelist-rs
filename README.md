# myanimelist-rs

An (unofficial) typesafe wrapper around the official MyAnimeList v2 API.

More information, including official documentation for the API can be found here.

[https://myanimelist.net/clubs.php?cid=13727](https://myanimelist.net/clubs.php?cid=13727)

# Authentication

MAL Uses Oauth2 authentication. This requires pre-registrations, which can be performed here
[https://myanimelist.net/apiconfig](https://myanimelist.net/apiconfig).

Oauth2 authentication can be a bit tricky, especially for native applications.
This library does not attempt to solve this generally. 
It does however provide some rudimentary helpers that are suitable for testing in a native setting.
It is however up to the application to implement an authentication flow suitable for its security
and convenience requirements.

For an example of what authentication flow might look like 
(using the provided non-general tools!)
look in `examples/auth.rs`

# API Use

Currently, only synchronous (blocking) requests 
(using the [reqwest library](https://github.com/seanmonstar/reqwest))
are implemented.

For examples on how to use the API, see `examples/sync.rs`.

# TODO

- [X] Basic Authentication
- [X] API Types (fully {de}serializable (via serde))
- [X] Synchronous API functions
- [ ] Aynchronous API functions
- [X] Unit test synchronous functions
- [ ] Unit test asynchronous functions
- [ ] Full documentation
- [ ] MAL Forums API

# CHANGELOG

## 0.1.x

### 0.1.0

- Initial release
- Rudimentary Oauth2 Authentication helpers
- Support all "Anime", "Manga", "User AnimeList", "User MangaList", and "User" API endpoints
- synchronous (blocking) requests via reqwests library