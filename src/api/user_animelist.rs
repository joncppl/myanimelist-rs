use crate::auth::Auth;
use super::Error;
use crate::api_objects::*;
use super::{perform_get, perform_patch, perform_delete, handle_response, BASE_URL};
use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
pub struct UpdateUserAnimeStatus {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<UserWatchStatus>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_rewatching: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub score: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub num_watched_episodes: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub priority: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub num_times_rewatched: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rewatch_value: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tags: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comments: Option<String>,
}

pub fn update_anime_list_status(anime_id: u64, update: &UpdateUserAnimeStatus, auth: &Auth) -> Result<UserAnimeListStatus, Error> {
    let response = perform_patch(
        &format!("{}/anime/{}/my_list_status", BASE_URL, anime_id),
        auth,
        update,
    )?;
    handle_response(&response)
}

pub fn delete_anime_from_list(anime_id: u64, auth: &Auth) -> Result<(), Error> {
    let response = perform_delete(
        &format!("{}/anime/{}/my_list_status", BASE_URL, anime_id),
        auth,
    )?;
    if response.status.is_success() {
        Ok(())
    } else {
        Err(Error::HttpError(response.status))
    }
}

#[derive(Clone, Debug, Serialize)]
pub struct GetUserAnimeListQuery {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<UserStatus>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sort: Option<SortStyle>,
    pub limit: u64,
    pub offset: u64,
    pub nsfw: bool,
}

pub fn get_user_anime_list<U: ToString>(user: U, query: &GetUserAnimeListQuery, auth: &Auth) -> Result<PageableData<Vec<Node<Anime>>>, Error> {
    let response = perform_get(
        &format!("{}/users/{}/animelist?{}",
                 BASE_URL,
                 user.to_string(),
                 serde_urlencoded::to_string(query)?
        ),
        auth)?;
    handle_response(&response)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_update_anime_list() {
        let auth = crate::auth::tests::get_auth_from_env();
        let query = UpdateUserAnimeStatus {
            status: Some(UserWatchStatus::Watching),
            is_rewatching: None,
            score: None,
            num_watched_episodes: Some(1),
            priority: None,
            num_times_rewatched: None,
            rewatch_value: None,
            tags: None,
            comments: None,
        };
        let anime_id = 41353; // https://myanimelist.net/anime/41353/The_God_of_High_School
        let result = update_anime_list_status(anime_id, &query, &auth).unwrap();
        println!("{:#?}", result);
        assert_eq!(result.num_episodes_watched, 1);
    }

    #[test]
    fn test_delete_anime_from_list() {
        let auth = crate::auth::tests::get_auth_from_env();
        let anime_id = 41353; // https://myanimelist.net/anime/41353/The_God_of_High_School
        delete_anime_from_list(anime_id, &auth).unwrap();
    }

    #[test]
    fn test_get_user_anime_list() {
        let auth = crate::auth::tests::get_auth_from_env();
        let query = GetUserAnimeListQuery {
            fields: Some(crate::api_objects::ALL_ANIME_AND_MANGA_FIELDS.to_string()),
            status: None,
            sort: None,
            limit: 100,
            offset: 0,
            nsfw: true,
        };
        let result = get_user_anime_list("@me", &query, &auth).unwrap();
        println!("{:#?}", result);
        assert!(result.data.len() > 0);
    }
}
