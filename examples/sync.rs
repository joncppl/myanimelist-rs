use myanimelist_rs::auth::Auth;

// Load your authentication in some way.
// see bin/auth.rs for an example authentication flow.
fn get_auth() -> Auth {
    let raw = std::env::var("MAL_TEST_AUTH").unwrap();
    let mut auth = serde_json::from_str::<Auth>(&raw).unwrap();
    let needs_refresh = if let Some(token) = &auth.token {
        token.expired()
    } else {
        panic!("\"MAL_TEST_AUTH\" has no token");
    };
    if needs_refresh {
        auth.refresh().unwrap();
    }
    auth
}

fn main() {
    let auth = get_auth();

    use myanimelist_rs::api_objects::*;
    use myanimelist_rs::api::*;

    // === Anime API ===

    // Effectively search for anime
    let query = anime::GetAnimeListQuery {
        q: "Code Geass".to_string(),
        limit: 2,
        offset: 0,
        nsfw: false,
        fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
    };
    let result = anime::get_anime_list(&query, &auth).unwrap();
    println!("{:#?}", result);

    // Get details on a specific anime by its MAL id
    let query = anime::GetAnimeDetailQuery {
        fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
        nsfw: false,
    };
    let anime_id = 1;
    let result = anime::get_anime_details(anime_id, &query, &auth).unwrap();
    println!("{:#?}", result);

    // List anime rankings
    let query = anime::GetAnimeRankingQuery {
        ranking_type: AnimeRankingType::All,
        limit: 100,
        offset: 0,
        nsfw: false,
        fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
    };
    let result = anime::get_anime_ranking(&query, &auth).unwrap();
    println!("{:#?}", result);

    // List anime that aired in a particular season
    let query = anime::GetSeasonalAnimeQuery {
        sort: None,
        limit: 100,
        offset: 0,
        nsfw: false,
        fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
    };
    let season = Season {
        year: 2020,
        season: SeasonIdent::Winter,
    };
    let result = anime::get_seasonal_anime(&season, &query, &auth).unwrap();
    println!("{:#?}", result);

    // List anime suggestions (for the authenticated user)
    let query = anime::GetSuggestedAnime {
        limit: 100,
        offset: 0,
        nsfw: false,
        fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
    };
    let result = anime::get_suggested_anime(&query, &auth).unwrap();
    println!("{:#?}", result);

    // === Manga API ===

    // Search for manga
    let query = manga::GetMangaListQuery {
        q: "Liar Game".to_string(),
        limit: 2,
        offset: 0,
        nsfw: false,
        fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
    };
    let result = manga::get_manga_list(&query, &auth).unwrap();
    println!("{:#?}", result);

    // Get manga details by it's MAL id
    let query = manga::GetMangaDetailQuery {
        fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
        nsfw: false,
    };
    let result = manga::get_manga_details(1649, &query, &auth).unwrap();
    println!("{:#?}", result);

    // List manga ranking
    let query = manga::GetMangaRankingQuery {
        ranking_type: MangaRankingType::All,
        limit: 100,
        offset: 0,
        nsfw: false,
        fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
    };
    let result = manga::get_manga_ranking(&query, &auth).unwrap();
    println!("{:#?}", result);

    // === User API ===

    // Get information about the authenticated user
    let query = user::GetMyUserInformationQuery {
        fields: Some(ALL_USER_FIELDS.to_string()),
    };
    let result = user::get_my_user_information(&query, &auth).unwrap();
    println!("{:#?}", result);

    // === User AnimeList API ===

    // Update (or Add) an entry to the authenticated user's anime list
    let query = user_animelist::UpdateUserAnimeStatus {
        status: Some(UserWatchStatus::Watching),
        is_rewatching: None,
        score: None,
        num_watched_episodes: Some(1),
        priority: None,
        num_times_rewatched: None,
        rewatch_value: None,
        tags: None,
        comments: None,
    };
    let anime_id = 41353; // https://myanimelist.net/anime/41353/The_God_of_High_School
    let result = user_animelist::update_anime_list_status(anime_id, &query, &auth).unwrap();
    println!("{:#?}", result);

    // Delete an entry from the authenticated user's anime list
    let anime_id = 41353; // https://myanimelist.net/anime/41353/The_God_of_High_School
    user_animelist::delete_anime_from_list(anime_id, &auth).unwrap();

    // get the authenticated user's anime list
    let query = user_animelist::GetUserAnimeListQuery {
        fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
        status: None,
        sort: None,
        limit: 100,
        offset: 0,
        nsfw: true,
    };
    let result = user_animelist::get_user_anime_list("@me", &query, &auth).unwrap();
    println!("{:#?}", result);

    // === User MangaList API ===

    // Update (or Add) an entry to the authenticated user's manga list
    let query = user_mangalist::UpdateUserMangaStatus {
        status: Some(UserReadStatus::Reading),
        is_rereading: None,
        score: None,
        num_volumes_read: Some(1),
        num_chapters_read: Some(2),
        priority: None,
        num_times_reread: None,
        reread_value: None,
        tags: None,
        comments: None,
    };
    let manga_id = 1; // https://myanimelist.net/manga/1/Monster
    let result = user_mangalist::update_manga_list_status(manga_id, &query, &auth).unwrap();
    println!("{:#?}", result);

    // Delete an entry from the authenticated user's manga list
    let manga_id = 1; // https://myanimelist.net/manga/1/Monster
    user_mangalist::delete_manga_from_list(manga_id, &auth).unwrap();

    // get the authenticated user's manga list
    let query = user_mangalist::GetUserMangaListQuery {
        fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
        status: None,
        sort: None,
        limit: 100,
        offset: 0,
        nsfw: true,
    };
    let result = user_mangalist::get_user_manga_list("@me", &query, &auth).unwrap();
    println!("{:#?}", result);
}