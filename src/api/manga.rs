use crate::auth::Auth;
use super::Error;
use crate::api_objects::*;
use super::{perform_get, handle_response, BASE_URL};
use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
pub struct GetMangaListQuery {
    pub q: String,
    pub limit: u64,
    pub offset: u64,
    pub nsfw: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<String>,
}

pub fn get_manga_list(query: &GetMangaListQuery, auth: &Auth) -> Result<PageableData<Vec<Node<Manga>>>, Error> {
    let response = perform_get(
        &format!("{}/manga?{}", BASE_URL, serde_urlencoded::to_string(query)?),
        auth,
    )?;
    handle_response(&response)
}

#[derive(Clone, Debug, Serialize)]
pub struct GetMangaDetailQuery {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<String>,
    pub nsfw: bool,
}

pub fn get_manga_details(manga_id: u64, query: &GetMangaDetailQuery, auth: &Auth) -> Result<Manga, Error> {
    let response = perform_get(
        &format!("{}/manga/{}?{}", BASE_URL, manga_id, serde_urlencoded::to_string(query)?),
        auth,
    )?;
    handle_response(&response)
}

#[derive(Clone, Debug, Serialize)]
pub struct GetMangaRankingQuery {
    pub ranking_type: MangaRankingType,
    pub limit: u64,
    pub offset: u64,
    pub nsfw: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<String>,
}

pub fn get_manga_ranking(query: &GetMangaRankingQuery, auth: &Auth) -> Result<PageableData<Vec<RankingMangaPair>>, Error> {
    let response = perform_get(
        &format!("{}/manga/ranking?{}", BASE_URL, serde_urlencoded::to_string(query)?),
        auth,
    )?;
    handle_response(&response)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_manga_list() {
        let auth = crate::auth::tests::get_auth_from_env();
        let query = GetMangaListQuery {
            q: "Liar Game".to_string(),
            limit: 2,
            offset: 0,
            nsfw: false,
            fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
        };
        let result = get_manga_list(&query, &auth).unwrap();
        println!("{:#?}", result);
        assert!(result.data.len() > 0);
    }

    #[test]
    fn test_get_manga_details() {
        let auth = crate::auth::tests::get_auth_from_env();
        let query = GetMangaDetailQuery {
            fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
            nsfw: false,
        };
        let result = get_manga_details(1649, &query, &auth).unwrap();
        println!("{:#?}", result);
        assert_eq!(result.title, "Liar Game");
    }

    #[test]
    fn test_get_manga_ranking() {
        let auth = crate::auth::tests::get_auth_from_env();
        let query = GetMangaRankingQuery {
            ranking_type: MangaRankingType::All,
            limit: 100,
            offset: 0,
            nsfw: false,
            fields: Some(ALL_ANIME_AND_MANGA_FIELDS.to_string()),
        };
        let result = get_manga_ranking(&query, &auth).unwrap();
        println!("{:#?}", result);
        assert!(result.data.len() > 0);
    }
}