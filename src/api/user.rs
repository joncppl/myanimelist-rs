use crate::auth::Auth;
use super::Error;
use crate::api_objects::*;
use super::{perform_get, handle_response, BASE_URL};
use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
pub struct GetMyUserInformationQuery {
    pub fields: Option<String>,
}

pub fn get_my_user_information(query: &GetMyUserInformationQuery, auth: &Auth) -> Result<UserInfo, Error> {
    let response = perform_get(
        &format!("{}/users/@me?{}", BASE_URL, serde_urlencoded::to_string(query)?),
        auth,
    )?;
    handle_response(&response)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_user_information() {
        let auth = crate::auth::tests::get_auth_from_env();
        let query = GetMyUserInformationQuery {
            fields: Some(ALL_USER_FIELDS.to_string()),
        };
        let result = get_my_user_information(&query, &auth).unwrap();
        println!("{:#?}", result);
    }
}
