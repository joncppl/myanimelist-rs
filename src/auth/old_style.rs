use serde::{Serialize, Deserialize};

/// For performing authorization
#[derive(Debug)]
pub struct Auth {
    /// The UserAgent of the application
    user_agent: String,
    /// The MAL client_id
    client_id: String,
    /// Client Username
    username: String,
    /// Client Password
    password: String,
}

/// An authorization token
#[derive(Clone, Debug)]
pub struct AuthToken {
    pub token_type: String,
    pub expires_in: u64,
    pub access_token: String,
    pub refresh_token: String,
}

/// An authorization failure
#[derive(Clone, Debug)]
pub struct AuthFailure {
    pub error: String,
    pub message: String,
}

/// Error during authorization
#[derive(Debug)]
pub enum AuthError {
    UnknownError,
    NetworkTimeout,
    InvalidResponse(String),
    AuthFailure(AuthFailure),
}

impl From<reqwest::Error> for AuthError {
    fn from(e: reqwest::Error) -> Self {
        if e.is_builder() || e.is_redirect() || e.is_status() {
            AuthError::UnknownError
        } else if e.is_timeout() {
            AuthError::NetworkTimeout
        } else {
            AuthError::UnknownError
        }
    }
}

const TOKEN_URL: &str = "https://api.myanimelist.net/v2/auth/token";
const CLIENT_ID_HEADER_NAME: &str = "X-MAL-Client-ID";

#[derive(Serialize, Debug)]
struct AuthBody<'a> {
    client_id: &'a str,
    password: &'a str,
    username: &'a str,
    grant_type: &'a str,
}

#[derive(Deserialize, Debug)]
struct AuthResponseSuccess {
    token_type: String,
    expires_in: u64,
    access_token: String,
    refresh_token: String,
}

#[derive(Deserialize, Debug)]
struct AuthResponseError {
    error: String,
    message: String,
}

impl Auth {
    /// Create a new auth object
    /// # Arguments
    /// * `user_agent` - The UserAgent of the application
    /// * `client_id` - Your MAL Client ID
    /// * `username` - Client Username
    /// * `password` - Client Password
    pub fn new<A: ToString, B: ToString, C: ToString, D: ToString>(
        user_agent: A,
        client_id: B,
        username: C,
        password: D,
    ) -> Self {
        Self {
            user_agent: user_agent.to_string(),
            client_id: client_id.to_string(),
            username: username.to_string(),
            password: password.to_string(),
        }
    }

    /// Perform the request for authorization
    pub fn request(&self) -> Result<AuthToken, AuthError> {
        let body = AuthBody {
            client_id: &self.client_id,
            password: &self.password,
            username: &self.username,
            grant_type: "password",
        };

        let request = reqwest::blocking::ClientBuilder::new()
            .user_agent(&self.user_agent)
            .build()
            .unwrap()
            .post(TOKEN_URL)
            .header(CLIENT_ID_HEADER_NAME, &self.client_id)
            .header(reqwest::header::ACCEPT, "application/json")
            .header(reqwest::header::CONTENT_TYPE, "application/x-www-form-urlencoded")
            .body(serde_urlencoded::to_string(&body).unwrap());

        let response = request.send()?;
        let success = response.status().is_success();
        let body = response.text()?;
        Self::handle_response(success, &body)
    }

    /// Perform the request for authorization (async)
    pub async fn request_async(&self) -> Result<AuthToken, AuthError> {
        let body = AuthBody {
            client_id: &self.client_id,
            password: &self.password,
            username: &self.username,
            grant_type: "password",
        };

        let request = reqwest::ClientBuilder::new()
            .user_agent(&self.user_agent)
            .build()
            .unwrap()
            .post(TOKEN_URL)
            .header(CLIENT_ID_HEADER_NAME, &self.client_id)
            .header(reqwest::header::ACCEPT, "application/json")
            .header(reqwest::header::CONTENT_TYPE, "application/x-www-form-urlencoded")
            .body(serde_urlencoded::to_string(&body).unwrap());

        let response = request.send().await?;
        let success = response.status().is_success();
        let body = response.text().await?;
        Self::handle_response(success, &body)
    }

    /// Process the authorization response
    pub fn handle_response(success: bool, body: &str) -> Result<AuthToken, AuthError> {
        if success {
            match serde_json::from_str::<AuthResponseSuccess>(&body) {
                Ok(auth_result) => Ok(AuthToken {
                    token_type: auth_result.token_type,
                    expires_in: auth_result.expires_in,
                    access_token: auth_result.access_token,
                    refresh_token: auth_result.refresh_token,
                }),
                Err(err) => { Err(AuthError::InvalidResponse(err.to_string())) }
            }
        } else {
            match serde_json::from_str::<AuthResponseError>(&body) {
                Ok(auth_result) => Err(AuthError::AuthFailure(AuthFailure {
                    error: auth_result.error,
                    message: auth_result.message,
                })),
                Err(err) => { Err(AuthError::InvalidResponse(err.to_string())) }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    struct Credentials {
        client_id: String,
        username: String,
        password: String,
    }

    fn get_env_credentials() -> Credentials {
        let client_id = std::env::var("MAL_TEST_CLIENT_ID");
        let username = std::env::var("MAL_TEST_USERNAME");
        let password = std::env::var("MAL_TEST_PASSWORD");

        if username.is_ok() && password.is_ok() && client_id.is_ok() {
            Credentials {
                client_id: client_id.unwrap(),
                username: username.unwrap(),
                password: password.unwrap(),
            }
        } else {
            panic!("MAL Tests required the environment variables \"MAL_TEST_CLIENT_ID\", \"MAL_TEST_USERNAME\" and \"MAL_TEST_PASSWORD\" to be set");
        }
    }

    // These are ignored b/c my client ID kept getting blocked when using this endpoint
    // esp. when using incorrect credentials

    #[test]
    #[ignore]
    fn test_network_general() {
        let request = reqwest::blocking::ClientBuilder::new()
            .user_agent("ACC")
            .build()
            .unwrap()
            .get("https://google.com");
        let response = request.send().unwrap();
        assert!(response.status().is_success());
    }

    #[test]
    #[ignore]
    fn test_mal_general() {
        let request = reqwest::blocking::ClientBuilder::new()
            .user_agent("ACC")
            .build()
            .unwrap()
            .get("https://myanimelist.net");
        let response = request.send().unwrap();
        assert!(response.status().is_success());
    }

    #[test]
    #[ignore]
    fn test_old_auth() {
        let credentials = get_env_credentials();
        Auth::new("AnimeCommandCenter", credentials.client_id, credentials.username, credentials.password).request().unwrap();
    }

    #[test]
    #[ignore]
    fn test_old_auth_bad_credentials() {
        let client_id = std::env::var("MAL_TEST_CLIENT_ID").unwrap();
        let username = "thisisnotarealusernameihope";
        let password = "butthisisdefinitelynotitspassword";
        if let Err(err) = Auth::new("AnimeCommandCenter", client_id, username, password).request() {
            match err {
                AuthError::AuthFailure(fail) => {
                    assert_eq!(fail.error, "invalid_grant")
                }
                _ => panic!()
            }
        } else {
            panic!();
        }
    }
}